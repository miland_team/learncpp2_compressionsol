#include <boost\dynamic_bitset\dynamic_bitset.hpp>
#include "zlib.h"

#pragma once

class CompressionHelper
{
private:
	boost::dynamic_bitset<Bytef> bytes_to_bitarray(std::vector<Bytef> v_bytes);
	std::vector<Bytef> bitarray_to_bytes(boost::dynamic_bitset<Bytef> bits);
	std::string bytes_to_string(std::vector<Bytef> v_bytes);

public:
	CompressionHelper();
	boost::dynamic_bitset<Bytef> read_file(std::string filename, long int& size);
	void write_file(std::string filename, boost::dynamic_bitset<Bytef> bits);
	boost::dynamic_bitset<Bytef> compress(std::string str);
	std::string decompress(boost::dynamic_bitset<Bytef> bits);
	~CompressionHelper();
};