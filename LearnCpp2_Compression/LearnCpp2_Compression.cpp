// LearnCpp2_Compression.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
//#include "zlib.h"
#include <boost\dynamic_bitset\dynamic_bitset.hpp>
#include "CompressionHelper.h"

using namespace std;

int main()
{
	string str_choose;
	string str_or_fname;
	cout << "Input (s)tring or (f)ile?" << endl;
	getline(cin, str_choose);

	CompressionHelper ch;
	if (str_choose[0] != 'f')
	{
		cout << "Input a string." << endl;
		getline(cin, str_or_fname);
		cout << "String length: " << str_or_fname.size() << endl;
		cout << "Compressing..." << endl;
		boost::dynamic_bitset<Bytef> bits = ch.compress(str_or_fname);
		// In bytes
		int comp_size = bits.size() / 8;
		cout << "String length: " << comp_size << endl;
		cout << "Decompressing..." << endl;
		string dec = ch.decompress(bits);
		cout << "Decompressed string: " << endl;
		cout << "---------------------" << endl;
		cout << dec << endl;
	}
	else
	{
		cout << "Input filename with path (i.e. C:\\...\\myFile.ext)." << endl;
		getline(cin, str_or_fname);
		cout << "Compressing..." << endl;
		long int size;
		boost::dynamic_bitset<Bytef> bits = ch.read_file(str_or_fname, size);
		cout << "Original size: " << size << endl;
		// In bytes
		int comp_size = bits.size() / 8;
		cout << "Compressed size: " << comp_size << endl;
		cout << "Decompressing..." << endl;
		ch.write_file(str_or_fname, bits);
		cout << "Check the decompressed file." << endl;
	}

	cout << "Press Enter to end.";
	getchar();

	return 0;
}

