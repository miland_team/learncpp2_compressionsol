#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <stdexcept>
#include <iterator>
#include <sstream>
#include <locale>
#include <fstream>
#include "StringHelper.h"


using namespace std;

ostream& StringHelper::formatDateTime(ostream& out, const tm& t, const char * fmt)
{
	const time_put<char>& dateWriter = use_facet<time_put<char> >(out.getloc());
	int n = strlen(fmt);
	if (dateWriter.put(out, out, ' ', &t, fmt, fmt + n).failed()) {
		throw runtime_error("failure to format date time");
	}
	return out;
}

string StringHelper::getFileName(std::string path)
{
	char sep = '/';

#ifdef _WIN32
	sep = '\\';
#endif

	size_t i = path.rfind(sep, path.length());
	if (i != string::npos) {
		return (path.substr(i + 1, path.length() - i));
	}

	return("");
}

string StringHelper::dateTimeToString(const char* format)
{
	time_t now = time(0);
	tm t = *localtime(&now);

	stringstream s;
	formatDateTime(s, t, format);
	return s.str();
}

// Returns a new file name with path for the input file
string StringHelper::getNewFileName(string input_filename)
{
	string pth = input_filename.substr(0, input_filename.find_last_of("\\/"));
	string fname = getFileName(input_filename);
	string current_date = dateTimeToString("%Y%m%d%H%M%S");
	string newFileName = pth + "\\test_" + current_date + "_" + fname;
	return newFileName;
}
