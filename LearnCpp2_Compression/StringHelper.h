#pragma once
class StringHelper
{
private:
	static std::ostream& formatDateTime(std::ostream& out, const tm& t, const char* fmt);

public:
	static std::string getFileName(std::string path);
	static std::string dateTimeToString(const char* format);
	static std::string getNewFileName(std::string input_filename);
};
