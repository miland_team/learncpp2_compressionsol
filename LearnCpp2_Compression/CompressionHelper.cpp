// Reference code for string compression/decompression
// https://panthema.net/2007/0328-ZLibString.html
// Adapted by Miland S.H.
// Distributed under the Boost Software License.
// (See http://www.boost.org/LICENSE_1_0.txt)

#define _CRT_SECURE_NO_DEPRECATE

#include <string>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include "StringHelper.h"
#include "CompressionHelper.h"

using namespace std;
using namespace boost;

#pragma region Private methods

// Convert a byte vector to a bit array (boost::dynamic_bitset)
dynamic_bitset<Bytef> CompressionHelper::bytes_to_bitarray(std::vector<Bytef> v_bytes)
{
	int num_bits = v_bytes.size() * 8;
	dynamic_bitset<Bytef> bit_array(num_bits);
	from_block_range(v_bytes.begin(), v_bytes.end(), bit_array);
	return bit_array;
}

// Convert a bit array (boost::dynamic_bitset) to a byte vector
std::vector<Bytef> CompressionHelper::bitarray_to_bytes(dynamic_bitset<Bytef> bits)
{
	std::vector<Bytef> bytes;
	to_block_range(bits, std::back_inserter(bytes));
	return bytes;
}

// Convert a byte vector to string
std::string CompressionHelper::bytes_to_string(std::vector<Bytef> v_bytes)
{
	std::ostringstream sstream;
	if (!v_bytes.empty())
	{
		// Convert all but the last element
		std::copy(v_bytes.begin(), v_bytes.end() - 1, std::ostream_iterator<Bytef>(sstream));
		// Close the stream adding last element without delimiter
		sstream << v_bytes.back();
	}
	std::string str = sstream.str();
	sstream.clear();
	return str;
}

#pragma endregion

#pragma region Public methods

CompressionHelper::CompressionHelper()
{
}

dynamic_bitset<Bytef> CompressionHelper::read_file(string filename, long int& size)
{
	const char* path = filename.c_str();
	// Reading size of file
	FILE * file = fopen(path, "rb");
	if (file == NULL)
		throw "File cannot be opened...";
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fclose(file);
	// Reading data to array of unsigned chars
	file = fopen(path, "rb");
	Bytef* in = new Bytef[size];
	int bytes_read = fread(in, sizeof(Bytef), size, file);
	fclose(file);

	std::string str(reinterpret_cast<char const*>(in), size);
	dynamic_bitset<Bytef> bits = compress(str);
	// Wipe 'in' from memory
	delete[] in;

	return bits;
}

void CompressionHelper::write_file(std::string filename, dynamic_bitset<Bytef> bits)
{
	StringHelper sh;
	string nfn = sh.getNewFileName(filename);
	const char* new_filename = nfn.c_str();
	string dec = decompress(bits);
	int size = dec.size();
	Bytef* buffer = (Bytef*)dec.c_str();

	FILE * file = fopen(new_filename, "wb");
	int bytes_written = fwrite(buffer, sizeof(Bytef), size, file);
	fclose(file);
}

dynamic_bitset<Bytef> CompressionHelper::compress(string str)
{
	int compressionlevel = Z_BEST_COMPRESSION;

	z_stream zstream;
	memset(&zstream, 0, sizeof(zstream));

	if (deflateInit(&zstream, compressionlevel) != Z_OK)
		throw(runtime_error("Deflate initialization failed while compressing."));

	zstream.next_in = (Bytef*)str.data();
	// set the z_stream's input
	zstream.avail_in = str.size();

	int ret_val;
	char buff_output[32768];
	string str_compressed;

	// Get the compressed bytes blockwise
	do {
		zstream.next_out = (Bytef*)buff_output;
		zstream.avail_out = sizeof(buff_output);

		ret_val = deflate(&zstream, Z_FINISH);

		// Block appened to the compressed string
		if (str_compressed.size() < zstream.total_out)
			str_compressed.append(buff_output, zstream.total_out - str_compressed.size());

	} while (ret_val == Z_OK);

	deflateEnd(&zstream);
	// Not EOF => error
	if (ret_val != Z_STREAM_END)
	{
		ostringstream sstream;
		sstream << "Exception during compression: (" << ret_val << ") " << zstream.msg;
		throw(runtime_error(sstream.str()));
	}
	// Convert the compressed string to a vetor
	std::vector<Bytef> bytes(str_compressed.begin(), str_compressed.end());
	// Convert to bit array
	dynamic_bitset<Bytef> bits = bytes_to_bitarray(bytes);

	return bits;
}

string CompressionHelper::decompress(dynamic_bitset<Bytef> bits)
{
	// Convert bit array to a byte vector
	std::vector<Bytef> bytes = bitarray_to_bytes(bits);
	// Convert byte vector to string
	std::string str = bytes_to_string(bytes);
	// Start decompression
	z_stream zstream;
	memset(&zstream, 0, sizeof(zstream));
	if (inflateInit(&zstream) != Z_OK)
		throw(runtime_error("Inflate initialization failed while decompressing."));
	// If ok continue
	zstream.next_in = (Bytef*)str.data();
	zstream.avail_in = str.size();

	int ret_val;
	char buff_output[32768];
	string str_output;

	// Get decompressed bytes block by block calling inflate multiple times
	do {
		zstream.next_out = (Bytef*)buff_output;
		zstream.avail_out = sizeof(buff_output);
		ret_val = inflate(&zstream, 0);
		if (str_output.size() < zstream.total_out)
			str_output.append(buff_output, zstream.total_out - str_output.size());
	} while (ret_val == Z_OK);

	inflateEnd(&zstream);

	// Not EOF => error
	if (ret_val != Z_STREAM_END)
	{
		std::ostringstream sstream;
		sstream << "Exception during decompression: (" << ret_val << ") " << zstream.msg;
		throw(runtime_error(sstream.str()));
	}

	return str_output;
}

CompressionHelper::~CompressionHelper()
{
}

#pragma endregion