# LearnCPP2_Compression #
This small project provides an implementation of string and file in memory compression.

**Important:** Being a memory compression it is not advisable to use for big amount of text or big files. *You have been warned.*

Compressed bytes are saved as bits using the *dynamic_bitset* provided by **boost**.
Decompressed bytes are saved as a string or a file.
This choice is because I needed this kind of stuff to be embedded in some sort of devices.

Why **boost** *dynamic_bitset* and not i.e. vector of bool or other likewise?
*Boost* library sounds good and worthy to be learnt, definitely by a newbie like myself.

Therefore, this implementation is mainly for learning purposes but not necessary close to it. It definitely can be expanded, i.e. writing some new methods to allow the compressed bytes to be save in a storage device such as hard disk or maybe a database.
If I can find the time I might add other methods, actually I wish to make it, but time... time... ticking away the moments that make up a dull day…

Having been for many years fasting of C++, actually mainly working on C#, I expect that the code is far from being perfect, but despite of that, it seems working fine.

This small project requires only two libraries:

i) **Zlib** ([http://www.zlib.net/](http://www.zlib.net/)) - using NuGet: Install-Package zlib.

* Used for compression and decompression.

ii) **Boost** ([http://www.boost.org/](http://www.boost.org/)) - using NuGet: Install-Package boost

* Used for the dynamic_bitset.

**Development tool**: *Microsoft Visual Studio 2015*.



**Side notes**

It is worth to mention that I have used some code (developed by Timo Bingmann, [https://panthema.net/2007/0328-ZLibString.html](https://panthema.net/2007/0328-ZLibString.html)) that I have a bit adapted, mainly to help myself to learn how it works.

The rest of the code is based on documentation found throughout the internet that I have collected and studied in order to implement the algorithms.

By the way, one thing I wish to point out is that despite mountains of documentation, here and there, are available, it is very difficult to find very clear examples on how to use properly the code that is shown.

Often, documentation of libraries lacks of good, simple and clear examples, very useful for programmers, like me, who haven't programmed in C++ for years and/or for just basic programmers as well.

Library documentation full of words, perfect description of methods, properties etc. but without good code examples often leads to an alley and basically useful for the few, for the insiders only.

I have always been taught that few code lines are better than 1000 words and useful for all.

Should you notice some weird things in code or have suggestions for possible improvements, kindly let me know.
I know I have still a lot to learn.

The code has been released according to the license policy reported below.

Thank you and good bytes.

>(C) 2016 MilAnd S.H.

> This project can be distributed under the Boost Software License.